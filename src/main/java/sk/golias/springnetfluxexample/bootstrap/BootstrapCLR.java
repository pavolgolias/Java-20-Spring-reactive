package sk.golias.springnetfluxexample.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import sk.golias.springnetfluxexample.domain.Movie;
import sk.golias.springnetfluxexample.repositories.MovieRepository;

import java.util.UUID;

@Component
public class BootstrapCLR implements CommandLineRunner {
    private final MovieRepository movieRepository;

    public BootstrapCLR(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public void run(String... args) {
        /*movieRepository.deleteAll().block();

        Flux.just("Silence of lambdas", "Enter the Mono", "Prince", "Meet the fluxes")
                .map(title -> new Movie(UUID.randomUUID().toString(), title))
                .flatMap(movieRepository::save)
                .subscribe(null, null, () -> {
                    movieRepository.findAll().subscribe(System.out::println);
                });*/

        movieRepository.deleteAll().thenMany(
                Flux.just("Silence of lambdas", "Enter the Mono", "Prince", "Meet the fluxes")
                        .map(Movie::new)
                        .flatMap(movieRepository::save))
                .subscribe(null, null, () -> movieRepository.findAll().subscribe(System.out::println));
    }
}
