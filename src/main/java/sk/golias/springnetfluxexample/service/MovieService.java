package sk.golias.springnetfluxexample.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.golias.springnetfluxexample.domain.Movie;
import sk.golias.springnetfluxexample.domain.MovieEvent;

public interface MovieService {

    Flux<MovieEvent> events(String movieId);

    Mono<Movie> getMovieById(String id);

    Flux<Movie> getAllMovies();
}
