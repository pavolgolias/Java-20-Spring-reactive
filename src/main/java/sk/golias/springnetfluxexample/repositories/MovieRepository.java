package sk.golias.springnetfluxexample.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import sk.golias.springnetfluxexample.domain.Movie;

public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {
}
